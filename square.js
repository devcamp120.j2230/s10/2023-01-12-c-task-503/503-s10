import { Retangle } from "./retangle.js";

// Tạo class hình vuông
// Hình vuông kế thừa hình chữ nhật (hình vuông cũng có width và height, phương thức getArea)
class Square extends Retangle {
    // Phát triển thêm các thuộc tính của riêng hình vuông
    description = "Đây là hình vuông";

    constructor(paramLength) {
        // Gọi đến phương thức khởi tạo của hình chữ nhật để set giá trị cho width và height
        super(paramLength, paramLength);
    }

    // Phát triển thêm các phương thức của riêng hình vuông
    getPerimeter() {
        return 4 * this.width;
    }
}

export { Square }
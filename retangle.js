// Tạo class hình chữ nhật
class Retangle {
    // Định nghĩa các thuộc tính
    width;
    height;

    // Phương thức khởi tạo
    constructor(paramWidth, paramHeight) {
        this.width = paramWidth;
        this.height = paramHeight;
    }

    // Phương thức của class
    getArea() {
        return this.width * this.height;
    }
}

export { Retangle }